# @x/log

Simple, fast, object based logging for the @x platform.

## Installation

```shell
yarn add @x/log
```

## Usage

```javascript
import logger from '@x/log'

const log = logger()
log.error(new Error('An error occurred'))
```

This creates a logger object that will use the default console log writer and only log entries with a level of 
`info` or higher.

## Log Functions

The created object exposes a function for each log level:

Level|Value
---|---
error|1 
warn|2
info|3
debug|4
trace|5

Each of these functions accepts a variable number of parameters that will be merged in to the final log entry. 
A `level` property will also be set based on the function called. String arguments will be assigned to the `message` 
property.

Additionally, the following utility functions are exposed:

### log(...args)

Log a new entry using the provided arguments without a level property.

### child(scope)

Create a child logger that will merge the provided object into each log entry.

## Options

The following options can be passed to the logger constructor:

Name|Type|Description
---|---|---
level|string/number|Only log messages with the level specified or higher
scope|object|An initial object to be merged in to each log entry
levels|object|A mapping of level names to values
filter|function|A function that is passed a log entry to determine whether or not to log
serializers|[object]|An array of objects containing `test` and `serialize` functions
writers|[object]|An array of writer constructor functions
timestampProperty|string|Property to attach current timestamp to. Set to falsy value to disable. Default: "timestamp"
attachUserAgent|boolean|Attach browser userAgent to logs
redirectConsole|boolean|Redirect any console output to the logger
mergeFields|string[]|An array of property names to be merged from log parameters into entry instead of overwriting
transform|function|Allows modification of the final log entry

## Static Module Properties

For convenience, the top level module exposes the following additional properties:

Name|Description
---|---
defaultOptions|The default configuration options
defaultWriter|The default log writer used
defaultSerializers|The default set of serializers used
writers|The available set of log writers

## Built In Writers

The following writers are available on the `writers` static property:

### `console`

The console writer will attempt to make use of native console logging features such as colorization and accepts the 
following options:

Name|Type|Default|Description
---|---|---|---
timestampProperty|string|timestamp|Property name to use to extract logged timestamp from
excludeProperties|[string]| |An array of property names to omit from display
filter|function(entry)| |An additional filter function specific to the writer
level|string| |An additional level filter specific to the writer

### `file`

The file writer simply outputs log entries to a specified file, one per line. It accepts the following options:

Name|Type|Default|Description
---|---|---|---
destination|string| |The file path to write to
rotate|boolean|false|Create a new file with the current date appended for each day

## Implementing Custom Writers

A writer implementation that can be passed to the `writers` collection should be a constructor function that is passed
the global options object and returns the actual writer function that is passed each log entry.

A typical implementation would look something like:

```javascript
const httpLogWriter = userOptions => globalOptions => {
  // providing userOptions introduces a new top level constructor - this is optional but used here to configure the URL
  // the entry has already been passed through relevant serializers, there is no need to deal with error objects
  return entry => fetch(userOptions.url, { method: 'POST', body: JSON.stringify(entry) })
}
```

This would be consumed like follows:

```javascript
const log = logger({
  writers: [httpLogWriter({ url: 'https://some.logging.domain/my/logging/endpoint' })]
})
```

## License

**The MIT License (MIT)**

Copyright © 2022 Dale Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
