const writer = require('./writers/console')
const levels = require('./levels')
const readline = require('readline')

const write = writer()({ levels })

const shell = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
})

shell.on('line', line => {
  try {
    write(JSON.parse(line))
  } catch({ message }) {
    write({ level: 1, message: `Unable to format log message: ${message}` })
  }
})
