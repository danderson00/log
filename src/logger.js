const consoleRedirect = require('./consoleRedirect')

const logger = module.exports = (options, scope = {}) => {
  const writers = options.writers.map(x => x(options))
  const levels = options.levels
  const resolveLevel = level => typeof level === 'number' ? level : levels[level]

  let currentLevel = resolveLevel(options.level)

  const serializeArgument = arg => {
    const serializer = options.serializers.find(x => x.test(arg))
    return serializer ? serializer.serialize(arg) : arg
  }

  const mergeFields = (fields, entry) => fields && Object.keys(fields).reduce(
    (result, fieldName) => ({
      ...result,
      [fieldName]: (entry.hasOwnProperty(fieldName) && options.mergeFields.includes(fieldName))
        ? (entry[fieldName] && entry[fieldName] instanceof Array)
          ? [...entry[fieldName], fields[fieldName]]
          : [entry[fieldName], fields[fieldName]]
        : fields[fieldName]
    }),
    {}
  )

  const resolveScope = () => Object.keys(scope).reduce(
    (resolved, key) => ({
      ...resolved,
      [key]: typeof scope[key] === 'function'
        ? scope[key]()
        : scope[key]
    }),
    {}
  )

  const createEntry = (level, args) => args.reduce(
    (entry, arg) => ({ ...entry, ...mergeFields(serializeArgument(arg), entry) }),
    {
      ...(options.timestampProperty && { [options.timestampProperty]: Date.now() }),
      ...((options.attachUserAgent && typeof window !== 'undefined') ? { userAgent: window.navigator.userAgent } : {}),
      ...resolveScope(),
      ...(level !== undefined && {
        level: levels.hasOwnProperty(level) ? levels[level] : level
      })
    }
  )

  const log = level => (...args) => {
    const entry = options.transform ? options.transform(createEntry(level, args)) : createEntry(level, args)

    if((levels[level] <= currentLevel) && options.filter(entry)) {
      writers.forEach(write => {
        try {
          write(entry)
        } catch(error) {
          // TODO: try to log failure to other writers, console.error as last resort
          console.error('Failed to log to writer: ', error)
        }
      })
      return entry
    }
  }

  const logFunctions = Object.keys(levels).reduce(
    (functions, level) => ({ ...functions, [level]: log(level) }),
    {}
  )

  const child = childScope => logger(options, {
    ...scope,
    ...(typeof childScope === 'string' ? { source: childScope } : childScope)
  })

  const setScope = newScope => scope = { ...scope, ...newScope }

  if(options.redirectConsole) {
    consoleRedirect(log)
  }

  return { ...logFunctions, log: log(), child, setScope }
}
