module.exports = {
  error: 1,
  warn: 2,
  info: 3,
  debug: 4,
  trace: 5
}
