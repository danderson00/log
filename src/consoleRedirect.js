module.exports = log => {
  console = {
    ...console,
    log: (...args) => log(undefined)(...args),
    error: (...args) => log('error')(...args),
    warn: (...args) => log('warn')(...args),
    info: (...args) => log('info')(...args),
    debug: (...args) => log('debug')(...args),
    trace: (...args) => log('trace')(...args),
  }
}
module.exports.originalConsole = console
module.exports.restore = () => console = module.exports.originalConsole
