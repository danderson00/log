const construct = require('./construct')
const console = require('./writers/console.node')
const file = require('./writers/file')
const raw = require('./writers/raw')

module.exports = construct(console, { console, file, raw })
