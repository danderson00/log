const createConsoleWriter = require('./console')

module.exports = createConsoleWriter(args => args, properties => properties)