const SonicBoom = require('sonic-boom')
const { existsSync, mkdirSync } = require('fs')
const { dirname, basename } = require('path')

module.exports = userWriterOptions => globalOptions => {
  const writerOptions = typeof userWriterOptions === 'string'
    ? { destination: userWriterOptions }
    : userWriterOptions

  const serialize = entry => JSON.stringify(entry)
  const stream = rotator(writerOptions)

  return entry => stream().write(`${serialize(entry)}\n`)
}

const currentDate = () => (date => (date.getFullYear() * 10000) + (date.getMonth() * 100) + date.getDate())(new Date())

const expandFilename = destination => (`${
  dirname(destination)}/${
  currentDate()}-${
  basename(destination)
}`)

const ensureDirectory = path => {
  if(!existsSync(dirname(path))) {
    mkdirSync(dirname(path))
  }
}

const rotator = ({ destination, rotate }) => {
  ensureDirectory(destination)

  const createStream = () => new SonicBoom({
    dest: rotate ? expandFilename(destination) : destination
  })

  let fileDateStamp = currentDate()
  let stream = createStream()

  return () => {
    const currentDateStamp = currentDate()

    if(rotate && fileDateStamp !== currentDateStamp) {
      stream.end()
      fileDateStamp = currentDateStamp
      stream = createStream()
    }

    return stream
  }
}
