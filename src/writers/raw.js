const { originalConsole } = require('../consoleRedirect')

module.exports = () => () => entry => originalConsole.log(JSON.stringify(entry))