const chalk = require('chalk')
const colorize = require('json-colorizer')
const createConsoleWriter = require('./console')
const colors = require('../colors')

module.exports = createConsoleWriter(
  (args, levelName) => [chalk[colors[levelName || 'info'].base](args.join(''))],
  (properties, levelName) => Object.keys(properties).length > 0
    ? '\n' + colorize(JSON.stringify(properties), { colors: colors[levelName || 'info'].json })
    : ''
)
