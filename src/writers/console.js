const { originalConsole } = require('../consoleRedirect')

const defaultOptions = {
  timestampProperty: 'timestamp',
  excludeProperties: []
}

module.exports = (formatArgs, formatProperties) => (userOptions = {}) => {
  let filter = userOptions.filter

  const globalTarget = typeof globalThis !== 'undefined' ? globalThis
    : typeof window !== 'undefined' ? window
    : typeof global !== 'undefined' ? global
    : this
  globalTarget._log = { setFilter: newFilter => filter = newFilter }

  return options => entry => {
    const writerOptions = { ...defaultOptions, ...userOptions }
    const levelName = Object.keys(options.levels).find(x => options.levels[x] === entry.level)
    const filterLevel = options.levels[writerOptions.level] || writerOptions.level

    if((!filterLevel || filterLevel >= entry.level) && (!filter || filter(entry))) {
      const args = formatEntry(levelName, entry, writerOptions, formatProperties)
      originalConsole[levelName || 'log'](...formatArgs(args, levelName))
    }
  }
}

// TODO: move to formatters
const pad = s => s.toString().padStart(2, '0')
const formatDate = d => `${
  d.getFullYear()}-${
  pad(d.getMonth() + 1)}-${
  pad(d.getDate())} ${
  pad(d.getHours())}:${
  pad(d.getMinutes())}:${
  pad(d.getSeconds())
}`

const formatEntry = (levelName = '', entry, options, formatProperties) => {
  const entryDate = entry[options.timestampProperty] ? new Date(entry[options.timestampProperty]) : new Date()
  const source = entry.source ? ` - ${entry.source}` : ''
  const stack = entry.stack ? '\n' + entry.stack.replace(/\\n/g, '\n') : ''
  const otherProperties = (({ level, message, stack, ...otherProperties }) => {
    const propertiesToDisplay = Object.keys(otherProperties).reduce(
      (properties, name) => ({
        ...properties,
        ...(!options.excludeProperties.includes(name) && name !== options.timestampProperty &&
          { [name]: entry[name] }
        )
      }),
      {}
    )
    return formatProperties(propertiesToDisplay, levelName)
  })(entry)
  return [
    `${
      formatDate(entryDate)}${
      levelName ? ` - ${levelName.toUpperCase()}` : ''}${
      source} - ${
      entry.message 
        ? typeof entry.message === 'string'
          ? entry.message
          : JSON.stringify(entry.message)
        : 'No message'}${
      stack
    }`,
    otherProperties
  ]
}
