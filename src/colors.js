module.exports = {
  debug: {
    base: 'gray',
    json: {
      BRACE: 'gray',
      BRACKET: 'gray',
      COLON: 'gray',
      COMMA: 'gray',
      STRING_KEY: 'magenta',
      STRING_LITERAL: 'yellow',
      NUMBER_LITERAL: 'green',
      BOOLEAN_LITERAL: 'cyan',
      NULL_LITERAL: 'white'
    }
  },
  info: {
    base: 'white',
    json: {
      BRACE: 'white',
      BRACKET: 'white',
      COLON: 'white',
      COMMA: 'white',
      STRING_KEY: 'magentaBright',
      STRING_LITERAL: 'yellowBright',
      NUMBER_LITERAL: 'greenBright',
      BOOLEAN_LITERAL: 'cyanBright',
      NULL_LITERAL: 'whiteBright'
    }
  },
  warn: {
    base: 'yellowBright',
    json: {
      BRACE: 'black',
      BRACKET: 'black',
      COLON: 'black',
      COMMA: 'black',
      STRING_KEY: 'magentaBright',
      STRING_LITERAL: 'yellowBright',
      NUMBER_LITERAL: 'greenBright',
      BOOLEAN_LITERAL: 'cyanBright',
      NULL_LITERAL: 'whiteBright'
    }
  },
  error: {
    base: 'redBright',
    json: {
      BRACE: 'black',
      BRACKET: 'black',
      COLON: 'black',
      COMMA: 'black',
      STRING_KEY: 'magentaBright',
      STRING_LITERAL: 'redBright',
      NUMBER_LITERAL: 'greenBright',
      BOOLEAN_LITERAL: 'cyanBright',
      NULL_LITERAL: 'whiteBright'
    }
  }
}