const construct = require('./construct')
const console = require('./writers/console.browser')
const raw = require('./writers/raw')

module.exports = construct(console, { console, raw })
