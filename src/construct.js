const logger = require('./logger')
const levels = require('./levels')
const defaultSerializers = require('./serializers')

module.exports = (defaultWriter, availableWriters) => {
  const applyOptions = (options = {}) => ({
    levels,
    level: 'warn',
    writers: [defaultWriter({ filter: options.filter })],
    filter: () => true,
    serializers: defaultSerializers,
    timestampProperty: 'timestamp',
    attachUserAgent: true,
    mergeFields: ['message', 'value'],
    ...options
  })

  const construct = (options = {}) => logger(applyOptions(options), options.scope)
  construct.defaultOptions = applyOptions()
  construct.defaultWriter = defaultWriter
  construct.defaultSerializers = defaultSerializers
  construct.writers = availableWriters
  return construct
}