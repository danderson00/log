module.exports = [
  {
    test: value => value instanceof Error,
    serialize: error => ({
      message: error.message,
      ...((typeof process !== 'object' || !process.env || process.env.NODE_ENV !== 'production') &&
        { stack: error.stack }
      ),
      code: error.code,
      data: error.data,
    })
  },
  {
    test: value => typeof value === 'string',
    serialize: message => ({ message })
  },
  {
    test: value => (typeof value === 'number' || value instanceof Array),
    serialize: value => ({ value })
  }
]