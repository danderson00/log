const xlog = require('..')
const browser = require('../src/browser')

let log, writer

const setup = options => {
  writer = jest.fn()
  log = xlog({ writers: [() => writer], timestampProperty: undefined, attachUserAgent: false, ...options })
}

test("executing constructor returns object with log functions", () => {
  expect(Object.keys(xlog())).toEqual(expect.arrayContaining(['error', 'warn', 'info', 'debug', 'child']))
})

test("executing log functions calls writer functions", () => {
  const writers = [jest.fn(), jest.fn()]
  const logger = xlog({ writers: writers.map(x => () => x), timestampProperty: undefined, attachUserAgent: false })
  logger.error()
  logger.error({ p1: 1 })
  expect(writers[0].mock.calls).toEqual([[{ level: 1 }], [{ level: 1, p1: 1 }]])
  expect(writers[1].mock.calls).toEqual([[{ level: 1 }], [{ level: 1, p1: 1 }]])
})

// test("log function omits level", () => {
//   const writer = jest.fn()
//   const logger = xlog({ writers: [() => writer] })
//   logger.log('test')
//   expect(writer.mock.calls).toEqual([[{ message: 'test' }]])
// })

test("scope is merged into log entries", () => {
  setup({ scope: { p1: 1 } })
  log.error()
  expect(writer.mock.calls).toEqual([[{ level: 1, p1: 1 }]])
})

test("scope keys that are functions are evaluated", () => {
  setup({ scope: { timestamp: () => 123456 } })
  log.error()
  expect(writer.mock.calls).toEqual([[{ level: 1, timestamp: 123456 }]])
})

test("child loggers accumulate scope", () => {
  setup({ scope: { p1: 1 } })
  log.child({ p2: 2 }).error()
  expect(writer.mock.calls).toEqual([[{ level: 1, p1: 1, p2: 2 }]])
})

test("child scope can be a string", () => {
  setup()
  log.child('source').error()
  expect(writer.mock.calls).toEqual([[{ level: 1, source: 'source' }]])
})

test("multiple arguments are merged into entry", () => {
  setup()
  log.error({ p1: 1 }, { p2: 2 })
  expect(writer.mock.calls).toEqual([[{ level: 1, p1: 1, p2: 2 }]])
})

test("entries with level lower than specified are ignored", () => {
  setup({ level: 'info' })
  log.debug()
  log.info()
  expect(writer.mock.calls).toEqual([[{ level: 3 }]])
})

test("errors are serialized", () => {
  setup()
  log.error(new Error('test'))
  log.error(new Error('test1'), new Error('test2'))
  expect(writer.mock.calls).toMatchObject([
    [{ message: 'test' }],
    [{ message: ['test1', 'test2'] }]
  ])
})

test("arrays, strings and numbers are serialized", () => {
  setup()
  log.error('test', 1)
  log.error(['test', 1])
  expect(writer.mock.calls).toEqual([
    [{ level: 1, message: 'test', value: 1 }],
    [{ level: 1, value: ['test', 1] }]
  ])
})

test("filter option specifies which messages to log", () => {
  setup({ filter: x => x.value > 1, level: 'debug' })
  log.info({ value: 1 })
  log.info({ value: 2 })
  log.debug({ value: 3 })
  expect(writer.mock.calls).toEqual([
    [{ level: 3, value: 2 }],
    [{ level: 4, value: 3 }]
  ])
})

test("browser implementation attaches userAgent if available", () => {
  // jest provides jsdom which includes navigator.userAgent
  const browserWriter = jest.fn()
  const browserLog = browser({ writers: [() => browserWriter], scope: { p1: 1 } })
  browserLog.error({ value: 1 })
  expect(browserWriter.mock.calls).toMatchObject([
    [{ level: 1, value: 1, p1: 1 }]
  ])
  expect(browserWriter.mock.calls[0][0].userAgent).toContain('jsdom')
})

test("specified fields are merged into array from multiple log arguments", () => {
  setup()
  log.error('test', 'test2')
  log.error({ message: 'test' }, 'test2')
  log.error({ message: ['test'] }, 'test2')
  log.error(1, 2, 3)
  log.error(undefined)
  log.error({ p1: 1 }, { p1: 2 })
  expect(writer.mock.calls).toEqual([
    [{ level: 1, message: ['test', 'test2'] }],
    [{ level: 1, message: ['test', 'test2'] }],
    [{ level: 1, message: ['test', 'test2'] }],
    [{ level: 1, value: [1, 2, 3] }],
    [{ level: 1 }],
    [{ level: 1, p1: 2 }]
  ])
  // an oddity that are difficult to deal with - multiple array values should be merged into a nested array
  // log.error([1, 2, 3], [4, 5, 6]) should result in { value: [[1, 2, 3], [4, 5, 6]] } but doesn't
})

test("transform option allows modification of final log entry", () => {
  setup({ transform: entry => ({ ...entry, transformed: true }) })
  log.error('test')
  expect(writer.mock.calls).toEqual([[
    { level: 1, message: 'test', transformed: true }
  ]])
})